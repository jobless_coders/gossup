package com.joblesscoders.gossup.chatactivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.joblesscoders.gossup.R;
import com.joblesscoders.gossup.pojo.Message;
import com.joblesscoders.gossup.pojo.User;
import com.joblesscoders.gossup.util.ClientAPI;
import com.joblesscoders.gossup.util.ClientAPI2;
import com.joblesscoders.gossup.util.Userhandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener {
    private ListView listView;
    private EditText message;
    private TextView name,time,name_logo;
    private View send;
    private Toolbar toolbar;
    private User user;
    private ClientAPI2 clientAPI;
    private ArrayList<Message> messageArrayList = new ArrayList<>();
    private MessageAdapter messageAdapter;
    private HandlerThread handlerThread;
    private boolean isAlive;
    private long timestamp = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        user = getIntent().getExtras().getParcelable("user");
        messageAdapter = new MessageAdapter(ChatActivity.this,messageArrayList);
        isAlive = true;

        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


   private void init() throws Exception {
       listView = findViewById(R.id.listview);
       listView.setDescendantFocusability(ListView.FOCUS_BLOCK_DESCENDANTS);
       listView.setAdapter(messageAdapter);
       time = findViewById(R.id.time);
       name = findViewById(R.id.name);
       name.setText(user.getUSERNAME());
       name_logo = findViewById(R.id.username_header);
       name_logo.setText(user.getUSERNAME().charAt(0)+"");
       message = findViewById(R.id.msg);
       toolbar = findViewById(R.id.toolbar);
       send = findViewById(R.id.send);
       setSupportActionBar(toolbar);
       getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       getSupportActionBar().setTitle("");
       send.setOnClickListener(this);
       if (Build.VERSION.SDK_INT >= 9) {
           StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                   .permitAll().build();
           StrictMode.setThreadPolicy(policy);
       }
       try {
           clientAPI = new ClientAPI2(true, this);
       } catch (Exception e) {
           e.printStackTrace();
       }
       handlerThread = new HandlerThread("Nigga");
       handlerThread.start();


       refreshData(0);

   }

    private void refreshData(int delay) {

        if(!isAlive) return;

        new Handler(handlerThread.getLooper()).postDelayed(new Runnable() {

            @Override
            public void run() {
                if(!isAlive) return;
                Log.e("AsyncChatThread","Running another refreshing procedure");
                try {
                    ArrayList<Message> temp = new ArrayList<>();
                    temp.addAll(clientAPI.getMessageAfterTime(user.getUSERNAME(),timestamp));
                   // Toast.makeText(ChatActivity.this, ""+temp.size(), Toast.LENGTH_SHORT).show();

                   // messageArrayList.clear();

                    messageArrayList.addAll(temp);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {


                            if(messageArrayList.size()>0&&messageArrayList.get(messageArrayList.size()-1).getTimeStamp()>timestamp) {
                                listView.smoothScrollToPosition(messageArrayList.size() - 1);
                                Toast.makeText(ChatActivity.this, "kl", Toast.LENGTH_SHORT).show();
                                timestamp = messageArrayList.get(messageArrayList.size()-1).getTimeStamp();
                                messageAdapter.notifyDataSetChanged();


                            }
                            if(!user.getSTATUS().equalsIgnoreCase("online")) {
                                time.setTextColor(getApplicationContext().getResources().getColor(R.color.grey));
                                time.setText(getPrettyDate(user.getSTATUS()));
                            }
                            else {
                                time.setTextColor(getApplicationContext().getResources().getColor(R.color.green));
                                time.setText(("Online"));
                            }
                        }
                    });

                    Log.e("AsyncChatThread",messageArrayList.size()+"");


                   /* if(allMessages[UI_state].size()==0){
                        setErrorMessage(errorMessages[UI_state]);
                    }
                    else setErrorMessage("");*/

                } catch (Exception e){
                    Log.e("AsyncChatThread","Exception "+e.getLocalizedMessage());
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//
//                        }
//                    });
                    // probably from the getRecent() or any of the methods
                }
                refreshData(1000);
            }
        }, delay);

    }


    @Override
    public void onClick(View v) {
        String msg = message.getText().toString().trim();
        if(msg.length()==0) {
            message.setText("");
            return;
        }

        try {

            clientAPI.sendMessage(user.getUSERNAME(),msg);
            message.setText("");

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    private String getPrettyDate(String time) {
        long dif = new Date().getTime() - new Date(Long.parseLong(time)).getTime();
        dif = dif/1000;
        //dif is now is seconds
        int secs[] = new int[]{0,60,3600,86400,604800,2592000,31536000};
        String units[] = new String[]{"sec","min","hr","day","week","mon","yr"};
        int c = 0;
        while(c < 7){
            if(dif == secs[c]){
                break;
            }else if(dif < secs[c]){
                c--;
                break;
            }
            c++;
        }
        int un =(c==0)?(int)dif:(int)(dif/secs[c]);
        return un + " " + units[c]+ ((un>1)?"s ago":" ago");
    }



    @Override
    public boolean onSupportNavigateUp() {

        onBackPressed();
        return super.onSupportNavigateUp();
    }
    public void closeConnection(){
        Log.e("niggachat","logout");
        isAlive = false;
        if(clientAPI!=null)
            clientAPI.closeConnection();

    }

    @Override
    public void onBackPressed() {
        closeConnection();
        super.onBackPressed();
    }
}
