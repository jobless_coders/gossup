package com.joblesscoders.gossup.chatactivity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.joblesscoders.gossup.R;
import com.joblesscoders.gossup.pojo.Message;
import com.joblesscoders.gossup.pojo.User;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class MessageAdapter extends ArrayAdapter<Message> {
    private Context context;
    private ArrayList<Message> chat_list;
    public static final int ME = 0;
    public static final int OTHER =1;

    public MessageAdapter(Context context, ArrayList<Message>  chats){
        super(context, 0, chats);
        chat_list = chats;
        this.context = context;
    }
    @Override
    public int getCount() {
        return chat_list.size();
    }

    @Nullable
    @Override
    public Message getItem(int position) {
        return chat_list.get(position);
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Message currentMessage = chat_list.get(position);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //View rowView = convertView;
        if(convertView == null){
            if(getItemViewType(position) == ME)
                convertView = inflater.inflate(R.layout.chat_bubble_view_sender, parent, false);
            else
                convertView = inflater.inflate(R.layout.chat_bubble_view_receiver, parent, false);
        }

        TextView message_body = (TextView) convertView.findViewById(R.id.message_body);
        TextView message_time = (TextView) convertView.findViewById(R.id.message_time);


        message_body.setText(currentMessage.getMessage());
       // message_time.setText(getPrettyDate(currentMessage.getTimeStamp()+""));

        String pattern = "hh:mm a";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        String date = simpleDateFormat.format(currentMessage.getTimeStamp());

        message_time.setText(date);
        return convertView;
    }

    private String getPrettyDate(String time) {
        long dif = new Date().getTime() - new Date(Long.parseLong(time)).getTime();
        dif = dif/1000;
        //dif is now is seconds
        int secs[] = new int[]{0,60,3600,86400,604800,2592000,31536000};
        String units[] = new String[]{"second","minute","hour","day","week","month","year"};
        int c = 0;
        while(c < 7){
            if(dif == secs[c]){
                break;
            }else if(dif < secs[c]){
                c--;
                break;
            }
            c++;
        }
        int un =(c==0)?(int)dif:(int)(dif/secs[c]);
        return un + " " + units[c]+ ((un>1)?"s ago":" ago");
    }


    @Override
    public int getItemViewType(int position) {
        if(chat_list.get(position).isSent())
            return ME;
        else
            return OTHER;

    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }
}
