package com.joblesscoders.gossup.chatdashboard;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.joblesscoders.gossup.R;
import com.joblesscoders.gossup.chatactivity.ChatActivity;
import com.joblesscoders.gossup.pojo.User;

import java.util.ArrayList;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ChatAdapter extends ArrayAdapter<User> {
private Context context;
private ArrayList<User> chat_list = new ArrayList();
private int style;

    public ChatAdapter(Context context, ArrayList<User>  chats, int style){
        super(context, 0, chats);
        chat_list = chats;
        this.context = context;
        this.style = style;
    }
    @Override
    public int getCount() {
        return super.getCount();
    }

    public void setStyle(int s){
        style = s;
    }


    @Nullable
    @Override
    public User getItem(int position) {
        return chat_list.get(position);
    }

   /* @Override
    public int getPosition(@Nullable User item) {
        return chat_list.get(item);
    }*/

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = convertView;
        if(convertView == null)
        rowView = inflater.inflate(R.layout.chat_header_view, parent, false);
        TextView first_letter = (TextView) rowView.findViewById(R.id.username_header);
        TextView username_display_text = (TextView) rowView.findViewById(R.id.username_display_text);
        TextView chat_head_last_seen = (TextView) rowView.findViewById(R.id.chat_header_last_seen);


        User currentUser = chat_list.get(position);
        first_letter.setText(currentUser.getUSERNAME().toUpperCase().charAt(0)+"");
        username_display_text.setText(currentUser.getUSERNAME());

       // if(style == 0)
       // {
            if(currentUser.getSTATUS().equalsIgnoreCase("online"))
            {
                chat_head_last_seen.setTextColor(context.getResources().getColor(R.color.green));
            chat_head_last_seen.setText("Online");
            }
            else
            {
                chat_head_last_seen.setTextColor(context.getResources().getColor(R.color.grey));
                String time = getPrettyDate(currentUser.getSTATUS());
                chat_head_last_seen.setText(time);
            }
        //}
        /*else{
            chat_head_last_seen.setText("");
        }*/
        return rowView;
    }

    private String getPrettyDate(String time) {
        long dif = new Date().getTime() - new Date(Long.parseLong(time)).getTime();
        dif = dif/1000;
        //dif is now is seconds
        int secs[] = new int[]{0,60,3600,86400,604800,2592000,31536000};
        String units[] = new String[]{"sec","min","hr","day","week","mon","yr"};
        int c = 0;
        while(c < 7){
            if(dif == secs[c]){
                break;
            }else if(dif < secs[c]){
                c--;
                break;
            }
            c++;
        }
        int un =(c==0)?(int)dif:(int)(dif/secs[c]);
        return un + " " + units[c]+ ((un>1)?"s ago":" ago");
    }
}
