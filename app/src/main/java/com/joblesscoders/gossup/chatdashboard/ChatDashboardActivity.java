package com.joblesscoders.gossup.chatdashboard;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.StrictMode;
import android.util.Log;
import android.os.StrictMode;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.joblesscoders.gossup.R;
import com.joblesscoders.gossup.chatactivity.ChatActivity;
import com.joblesscoders.gossup.login.LoginActivity;
import com.joblesscoders.gossup.pojo.User;
import com.joblesscoders.gossup.util.ClientAPI;
import com.joblesscoders.gossup.util.ClientAPI2;
import com.joblesscoders.gossup.util.Userhandler;

import java.io.IOException;
import java.util.ArrayList;

public class ChatDashboardActivity extends AppCompatActivity implements View.OnClickListener {
public ListView chatList;
public ChatAdapter adapter;
public ArrayList<User> recent, online, all,chat_list;
private ClientAPI2 clientAPI;
public int UI_state;
public TextView empty_message;
public ArrayList[] allMessages;
public String[] errorMessages;
HandlerThread handlerThread;
private boolean isAlive;
View logout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_dashboard);
        if (Build.VERSION.SDK_INT >= 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        isAlive = true;
        errorMessages = new String[]{
                "You don't have any recent chats",
                "No users are online currently",
                "Nobody uses our service :("
        };

        empty_message = findViewById(R.id.dashboard_empty_message);
        logout = findViewById(R.id.logout);
        logout.setOnClickListener(this);
        chat_list = new ArrayList<>();
        chatList = findViewById(R.id.chat_list);
        adapter = new ChatAdapter(ChatDashboardActivity.this, chat_list, 0);
        chatList.setAdapter(adapter);
        //chatList.setDescendantFocusability(ListView.FOCUS_BLOCK_DESCENDANTS);
        chatList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               // Toast.makeText(ChatDashboardActivity.this, "nigga", Toast.LENGTH_SHORT).show();
               //close();
                Intent intent = new Intent(ChatDashboardActivity.this, ChatActivity.class);
                intent.putExtra("user",chat_list.get(position));
                startActivity(intent);
            }
        });

        UI_state = 0;

        recent = new ArrayList<>();
        online = new ArrayList<>();
        all = new ArrayList<>();

        allMessages = new ArrayList[3];
        allMessages[0] = recent;
        allMessages[1] = online;
        allMessages[2] = all;

        try {
         clientAPI = new ClientAPI2(true, this);
        } catch (Exception e) {
            e.printStackTrace();
        }


        handlerThread = new HandlerThread("ChatRefreshThread");
        handlerThread.start();


        refreshData(0);
    }

    @Override
    protected void onPause() {

        close();
        super.onPause();
    }

    @Override
    protected void onResume() {

        try {
            clientAPI = new ClientAPI2(true, this);
            Log.e("niggadash","login");
            //Toast.makeText(this, "kk", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e("ChatDash","Cannot reopen socket in onResume: "+e.getMessage());
        }
        isAlive = true;
        refreshData(1);
        super.onResume();
    }



    private void refreshData(int delay) {
        //load this 3 arraylists from socket data
       // chat_list = new ArrayList<>();
//        recent.add(new User("who are you?","online",false));

        if(!isAlive)
        {

            return;
        }
        new Handler(handlerThread.getLooper()).postDelayed(new Runnable() {

            @Override
            public void run() {
                if(!isAlive) return;
                Log.e("AsyncThread","Running another refreshing procedure");
                try {


                    getRecentUsers();
                    getAllUsers();
                    getActiveUsers();

                    if(allMessages[UI_state].size()==0){
                        setErrorMessage(errorMessages[UI_state]);
                    }
                    else //setErrorMessage("");
                    resetErrorMessage();

                } catch (Exception e) {
                    Log.e("AsyncThread","IOException "+e.getLocalizedMessage());
                    setErrorMessage("Server error!");
                    clientAPI = null;

                }  // probably from the getRecent() or any of the methods

                finally {
                    if(clientAPI == null){
                        try{
                            clientAPI = new ClientAPI2(true, getApplicationContext());
                            Log.e("niggadash","login2");
                        }catch (Exception e){
                            Log.e("AsyncThread","Fatally fatal error, "+e.getMessage());
                        }
                    }
                }
                refreshData(4000);
            }
        }, delay);

    }

    public void changeUIState(View v){
        int newState = 0;
        switch (v.getId()){
//            case R.id.recent_bar:
            case R.id.recent_text:
                newState = 0;
                break;
//            case R.id.online_bar:
            case R.id.online_text:
                newState = 1;
                break;
//            case R.id.all_bar:
            case R.id.all_text:
                newState = 2;
                break;
        }
        if(newState!=UI_state){
            UI_state = newState;
            UI_state = newState;
            inflateState();
        }
    }

    @Override
    protected void onDestroy() {
        close();
        super.onDestroy();

    }

    private void setErrorMessage(final String message){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                empty_message.setVisibility(View.VISIBLE);
                empty_message.setText(message);
            }
        });
    }

    private void setColor(int id, int color){
        findViewById(id).setBackgroundColor(getResources().getColor(color));
    }

    private void setAlpha(int id, float alpha){
        findViewById(id).setAlpha(alpha);
    }
    private void inflateState() {
        setColor(R.id.recent_bar, R.color.white);
        setColor(R.id.online_bar, R.color.white);
        setColor(R.id.all_bar, R.color.white);

        setAlpha(R.id.recent_text, 0.6f);
        setAlpha(R.id.online_text, 0.6f);
        setAlpha(R.id.all_text, 0.6f);

        adapter.setStyle(UI_state);
        resetErrorMessage();
        //setErrorMessage("");
        chatList.setSelection(0);
        switch (UI_state){
            case 0:
                setColor(R.id.recent_bar, R.color.black);
                setAlpha(R.id.recent_text, 1.0f);
                chat_list.clear();
               chat_list.addAll(recent);
               adapter.notifyDataSetChanged();
                if(recent.size()==0)
                    setErrorMessage("You don't have any friends");
                break;
            case 1:
                setColor(R.id.online_bar, R.color.black);
                setAlpha(R.id.online_text, 1.0f);
                chat_list.clear();
                chat_list.addAll(online);
                adapter.notifyDataSetChanged();
                if(online.size()==0)
                    setErrorMessage("No users are online currently");
                break;
            case 2:
                setColor(R.id.all_bar, R.color.black);
                setAlpha(R.id.all_text, 1.0f);
                chat_list.clear();
                chat_list.addAll(all);
                adapter.notifyDataSetChanged();
                if(all.size()==0)
                    setErrorMessage("Nobody uses our service :(");
                break;
        }

        chatList.setSelection(0);
    }

   public void getRecentUsers() throws Exception {
        recent.clear();
        recent.addAll(clientAPI.getRecentUserDetails());
        for(User user:recent) {
            if (user.getUSERNAME().equals(Userhandler.getUsername(getApplicationContext()))) {
                recent.remove(user);
            break;
            }
        }
        Log.e("ChatDash","recent got "+recent.size()+" elements");
        if(UI_state == 0)
            updateList(recent);



    }
    public void updateList(ArrayList<User> arrayList1){
        final ArrayList<User> arrayList = arrayList1;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                chat_list.clear();
                chat_list.addAll(arrayList);
                Log.e("ChatDash","list updated with "+arrayList.size()+" elements");
                adapter.setStyle(UI_state);
                adapter.notifyDataSetChanged();
            }
        });
    }
    public void getAllUsers() throws Exception {
        all.clear();
        all.addAll(clientAPI.getAllUserDetails());
        for(User user:all) {
            if (user.getUSERNAME().equals(Userhandler.getUsername(getApplicationContext()))) {
                all.remove(user);
                break;
            }
        }
        if(UI_state == 2)
            updateList(all);

    }
    public void getActiveUsers() throws Exception {
        online.clear();
        online.addAll(clientAPI.getOnlineUserDetails());
        for(User user:online) {
            if (user.getUSERNAME().equals(Userhandler.getUsername(getApplicationContext()))) {
                online.remove(user);
                break;
            }
        }
        if(UI_state == 1)
            updateList(online);


    }

    public void close(){
       // Toast.makeText(this, "closed", Toast.LENGTH_SHORT).show();
        Log.e("niggadash","logout");
           if(clientAPI!=null)
        clientAPI.closeConnection();
        isAlive = false;
    }

    @Override
    public void onClick(View v) {
        new AlertDialog.Builder(this)

                .setMessage("Do you really want to log out?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            logOutFromApp();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }).setNegativeButton("Nope", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        }).show();


    }
    public void resetErrorMessage(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                empty_message.setVisibility(View.GONE);
                empty_message.setText("");
            }
        });
    }

    private void logOutFromApp() {
      //  close();
        SharedPreferences settings = getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
        settings.edit().clear().commit();
        startActivity(new Intent(this,LoginActivity.class));
        finish();

    }
}
