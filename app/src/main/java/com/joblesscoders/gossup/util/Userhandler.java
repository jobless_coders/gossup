package com.joblesscoders.gossup.util;

import android.content.Context;
import android.content.SharedPreferences;

public class Userhandler {
    public static String getUsername(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("UserInfo",Context.MODE_PRIVATE);
       return sharedPreferences.getString("username",null);
    }
    public static String getPassword(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("UserInfo",Context.MODE_PRIVATE);
        return sharedPreferences.getString("password",null);
    }
    public static void setUsername(Context context,String username){
        SharedPreferences sharedPreferences = context.getSharedPreferences("UserInfo",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
         editor.putString("username",username);
         editor.commit();

    }
    public static void setPassword(Context context,String password){
        SharedPreferences sharedPreferences = context.getSharedPreferences("UserInfo",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("password",password);
        editor.commit();

    }
}
