package com.joblesscoders.gossup.pojo;

import com.joblesscoders.gossup.util.ClientAPI;

public class Message {
    public String getUserInvolved() {
        return userInvolved;
    }

    public void setUserInvolved(String userInvolved) {
        this.userInvolved = userInvolved;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public boolean isSent() {
        return isSent;
    }

    public void setSent(boolean sent) {
        isSent = sent;
    }

    String userInvolved, message;
    long timeStamp;
    boolean isSent;

    public Message(String username, String time, String msg, String isSendType) {
        userInvolved = username;
        timeStamp = Long.parseLong(time);
        message = msg;
        isSent = isSendType.equals(ClientAPI.YES);
    }
}



