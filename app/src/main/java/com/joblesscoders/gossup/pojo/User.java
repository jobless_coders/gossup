package com.joblesscoders.gossup.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.joblesscoders.gossup.util.ClientAPI;


public class User implements Parcelable {
    private String USERNAME, STATUS;
    private boolean HAS_CHATTED_BEFORE;

    public User(String name, String status, boolean chattedBefore) {
        USERNAME = name;
        STATUS = status;
        HAS_CHATTED_BEFORE = chattedBefore;
    }
    public User(String name, String status, String chattedBefore) {
        USERNAME = name;
        STATUS = status;
        HAS_CHATTED_BEFORE = chattedBefore.equals(ClientAPI.YES);
    }

    protected User(Parcel in) {
        USERNAME = in.readString();
        STATUS = in.readString();
        HAS_CHATTED_BEFORE = in.readByte() != 0;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getUSERNAME() {
        return USERNAME;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public boolean isHAS_CHATTED_BEFORE() {
        return HAS_CHATTED_BEFORE;
    }

    public void setUSERNAME(String USERNAME) {
        this.USERNAME = USERNAME;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public void setHAS_CHATTED_BEFORE(boolean HAS_CHATTED_BEFORE) {
        this.HAS_CHATTED_BEFORE = HAS_CHATTED_BEFORE;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(USERNAME);
        dest.writeString(STATUS);
        dest.writeByte((byte) (HAS_CHATTED_BEFORE ? 1 : 0));
    }
}


