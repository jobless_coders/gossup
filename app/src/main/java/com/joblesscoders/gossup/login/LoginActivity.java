package com.joblesscoders.gossup.login;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.joblesscoders.gossup.chatdashboard.ChatDashboardActivity;
import com.joblesscoders.gossup.R;
import com.joblesscoders.gossup.util.ClientAPI;
import com.joblesscoders.gossup.util.ClientAPI2;
import com.joblesscoders.gossup.util.Userhandler;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    EditText username;
    TextInputEditText password;
    MaterialButton login;
    String username_value, password_value;
    ClientAPI2 clientAPI;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
       if( Userhandler.getUsername(getApplicationContext()) != null) {
           Intent intent = new Intent(LoginActivity.this,ChatDashboardActivity.class);
           startActivity(intent);
           finish();
       }
       if (Build.VERSION.SDK_INT >= 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);

        }

        try {
            Init();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Login",e.getMessage());
            //Toast.makeText(this, "Login failed!", Toast.LENGTH_SHORT).show();
        }

    }

    public void Init() throws Exception {
        username = findViewById(R.id.username);
        login = findViewById(R.id.login);
        password = findViewById(R.id.password);
        login.setOnClickListener(this);
        clientAPI = new ClientAPI2(false, this);
    }

    public void createNewUser() throws Exception {
        String res = clientAPI.attemptLogin(username_value, password_value, true);
        if (res.equals(ClientAPI.SUCCESSFUL_LOGIN))
            handleSuccesfullLogin();

    }

    public void handleUserNotFound() {
        new AlertDialog.Builder(this)
                .setTitle("User not found")
                .setMessage("User not found, do you want to create new account using this username and password ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            createNewUser();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }).setNegativeButton("Nope", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        }).show();

    }

    public void handleSuccesfullLogin() throws Exception {
        Toast.makeText(this, "Logged in successfully", Toast.LENGTH_SHORT).show();
        Userhandler.setUsername(LoginActivity.this, username_value);
        Userhandler.setPassword(LoginActivity.this, password_value);
        //getRecentUsers();
        clientAPI.closeConnection();
        Intent intent = new Intent(LoginActivity.this, ChatDashboardActivity.class);

        clientAPI.closeConnection();
        startActivity(intent);
        finish();

    }
    public void getRecentUsers() throws Exception {
        Toast.makeText(this, ""+clientAPI.getOnlineUserDetails().size()+" niggas online", Toast.LENGTH_SHORT).show();



    }

    public void authOverride(View v){
        clientAPI.closeConnection();
        Intent intent = new Intent(LoginActivity.this,ChatDashboardActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        clientAPI.closeConnection();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login:
                username_value = username.getText().toString().trim();
                password_value = password.getText().toString().trim();
                if (username_value.length() == 0)
                    Toast.makeText(this, "Username cannot be empty", Toast.LENGTH_SHORT).show();
                if (username_value.contains(" "))
                    Toast.makeText(this, "Space not allowed in username", Toast.LENGTH_SHORT).show();
                else if (password_value.length() < 8)
                    Toast.makeText(this, "Password should have minimum 8 characters", Toast.LENGTH_SHORT).show();
                else {
                    try {
                        sendLoginRequest();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                break;
            default:
                break;
        }
    }

    private void sendLoginRequest() throws Exception {
        String response = clientAPI.attemptLogin(username_value, password_value);
        switch (response) {
            case ClientAPI.USERNAME_NOT_FOUND_CREATE:
                handleUserNotFound();


                break;
            case ClientAPI.SUCCESSFUL_LOGIN:
                handleSuccesfullLogin();
                break;
            case ClientAPI.WRONG_PASSWORD:
                Toast.makeText(this, "Wrong Password", Toast.LENGTH_SHORT).show();
                password.setText("");
                password_value = "";
                break;
        }

    }

   /* private class AsyncAction extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... args) {
            try {
                clientSide = new ClientSide(false, LoginActivity.this);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String result) {
            //resultis the data returned from doInbackground

        }
    }*/
}
